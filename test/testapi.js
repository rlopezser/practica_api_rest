var mocha = require ('mocha');
var chai = require ('chai');
var chaihttp = require ('chai-http');

//aumentamnos el objeto chai con chaihttp;
chai.use (chaihttp);

//Seleccionamos las aserciones a usar de chai. En este caso "should", lo que hemos ejecutado da como resultado
//lo que esperamos que dé como resultado
var should = chai.should();

var server = require ('../server.js');

//Definimos el primer test de prueba
describe ('First test suite',
  function(){
      it('Text that DouckDuckGo works',
        function(done){
          // test para probar un api rest a una dirección cualquiera
          chai.request('http://www.duckduckgo.com')
            .get('/')
            .end(
              function(err,res){
                console.log("Request has ended");
                console.log(err);
                //console.log(res);
                res.should.have.status(200);
                //done(); significa que termina esta función y en este punto se comprobarían las aserciones
                done();
              }
            );
        }
      );
  }
);

//Definimos el test de nuesta api
/*describe ('Test de API de usuarios Tech U',
  function(){
      it('Prueba que la API de usuarios funciona correctamente',
        function(done){
          // test para probar un api rest a una dirección cualquiera
          chai.request('http://localhost:3000')
            .get('/apitechu/v1')
            .end(
              function(err,res){
                console.log("Request has ended");
                //console.log(res);
                res.should.have.status(200);
                res.body.msg.should.be.eql("hola");
                //done(); significa que termina esta función y en este punto se comprobarían las aserciones
                done();
              }
            );
        }
      );
  }
);*/

describe ('Test de API de usuarios',
  function(){
      it('Prueba que la API de usuarios funciona correctamente',
        function(done){
          // test para probar un api rest a una dirección cualquiera
          chai.request('http://localhost:3000')
            .get('/apitechu/v1')
            .end(
              function(err,res){
                console.log("Request has ended");
                //console.log(res);
                res.should.have.status(200);
                res.body.msg.should.be.eql("hola");
                //done(); significa que termina esta función y en este punto se comprobarían las aserciones
                done();
              }
            );
        }
      ),
      it('Prueba que la API devuelve una lista de usuarios correctos',
        function(done){
          // test para probar un api rest a una dirección cualquiera
          chai.request('http://localhost:3000')
            .get('/apitechu/v1/users')
            .end(
              function(err,res){
                console.log("Request has ended");
                //console.log(res);
                res.should.have.status(200);
                res.body.should.be.a("array");
                for (user of res.body)
                {
                  user.should.have.property('email');
                  user.should.have.property('password');
                }
                done();
              }
            );
        }
      );
  }
);
