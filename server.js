//Import
var express = require ('express');

//Iniciación
var app = express();

//Me permite parsear el cuerpo de una petición post, el body del postman
var bodyParser = require ('body-parser');
app.use(bodyParser.json());

//Variable para el consumo de api de mlab
var baseMLabURL = 'https://api.mlab.com/api/1/databases/apitechurls5ed/collections/';
var mLabApiKey = 'apiKey=dZCbTEj9NfONsPhQD4QbLSIbKG11OWpT';

//Dependencia para gestionar peticiones http
var requestJson = require ('request-json');

//También por covencion el puerto que se esté usando o en su defecto el 3000
var port = process.env.PORT || 3000;

//En qué puerto escuecha nuestra aplicación
app.listen (port);

console.log ("API escuchando en el puerto " + port);

//Definimos una ruta "get" con su correspondiente función enlazadora (callback)
app.get ("/apitechu/v1",
  function (req,res){
      console.log("GET /apitechu/v1");
      res.send({"msg":"hola"});
  }
);

app.get ("/apitechu/v1/users",
  function(req,res){
      console.log("GET /apitechu/v1/users");
      //res.sendFile('./usuarios.json'); //Deprecated

      //Una forma de recuperar un fichero
      //res.sendFile('usuarios.json', {root:__dirname});

      //Otra forma de recuperar un fichero
      var users = require('./usuarios.json');
      res.send(users);
  }
);

/*app.post ("/apitechu/v1/users",
  function(req,res){
      console.log("POST /apitechu/v1/users");
      var newUser = {
        "first_name": req.headers.first_name,
        "last_name": req.headers.last_name,
        "country": req.headers.country
      }
      var users = require('./usuarios.json');
      users.push (newUser);
      writeUserDataToFile(users);
      console.log("Usuario añadido con exito");
      res.send({"msg":"Usuario añadido con exito"});
    }
);*/

app.post ("/apitechu/v1/users",
  function(req,res){
      console.log("POST /apitechu/v1/users");
      var newUser = {
        "first_name": req.body.first_name,
        "last_name": req.body.last_name,
        "country": req.body.country
      }
      var users = require('./usuarios.json');
      users.push (newUser);
      writeUserDataToFile(users);
      console.log("Usuario añadido con exito");
      res.send({"msg":"Usuario añadido con exito"});
    }
)

app.delete ("/apitechu/v1/users/:id",
  function(req,res){
    console.log("DELETE /apitechu/v1/users/:id");
    console.log(req.params.id);
    var users = require('./usuarios.json');

    //Corta un trozo de un array, se posiciona en la posición x y quita n elementos
    users.splice(req.params.id-1,1);
    writeUserDataToFile(users);
    console.log("usuario borrado");
    res.send ({"msg":"Usuario borrado con exito"});
  }
);

app.post("/apitechu/v1/monstruo/:p1/:p2",
 function (req, res) {
   console.log("Parametros");
   console.log(req.params);

   console.log("Query String");
   console.log(req.query);

   console.log("Body");
   console.log(req.body);

   console.log("Headers");
   console.log(req.headers);
 }
);

app.post ("/apitechu/v1/login",
  function(req,res){
      console.log("POST /apitechu/v1/login");
      var newUser = {
        "email": req.body.email,
        "password": req.body.password,
      }
      var users = require('./usuarios_login.json');
      var id;
      var encontrado = false;
      for (user of users)
      {
        //console.log(user);

        if (user.email == newUser.email && user.password == newUser.password)
        {
          encontrado = true;
          user.logged = true;
          id = user.id;
          //console.log (id);
          writeUserDataToFile(users);
        }
      }
      if (encontrado)
      {
        res.send ({"mensage":"login correcto", "idUsuario": id});
      }
      else {
        res.send ({"mensage":"login incorrecto"});
      }
    }
)

app.post ("/apitechu/v2/login",
  function(req,res){
      console.log("POST /apitechu/v2/login");
      var httpClient = requestJson.createClient(baseMLabURL);
      var emailUser = req.body.email;
      var passUser = req.body.password;
      var query = 'q={"email":"' + emailUser + '", "password":"'+passUser+'"}';
      var bEncontrado = false;
      var response;

      httpClient.get("user?" +  query + "&" +  mLabApiKey,
        function(err, resMLab, body){
          if (err)
          {
            response = {
                "msg" : "Error obteniendo usuario"
            };
            res.status(500);
            res.send(response);
          } else {
            if (body.length <=0){
              response = {
                "msg" : "login incorrecto"
              };
              res.status(404);
              res.send(response);
            }
            else {
              var putBody = '{"$set":{"logged":true}}';
              httpClient.put("user?" + query + "&" + mLabApiKey, JSON.parse(putBody),function(err, resPutMlab, body2){
                var jsonUserData = body[0];
                if (err)
                {
                  response = {
                      "msg" : "Error marcando usuario logado"
                  };
                  res.status(500);
                } else {
                  console.log ("login correcto");
                  response = {
                      "msg" : "login correcto",
                      "idUsuario" : jsonUserData.id
                  };
                }
                res.send(response);
              });
            }
          }
        }
      );
    }
)


app.post ("/apitechu/v2/logout",
  function(req,res){
      console.log("POST /apitechu/v2/logout");
      var httpClient = requestJson.createClient(baseMLabURL);
      var id = req.body.id;
      var query = 'q={"id":' + id + ',"logged": true}';
      var response;

      httpClient.get("user?" +  query + "&" +  mLabApiKey,
        function(err, resMLab, body){
          if (err)
          {
            response = {
                "msg" : "Error obteniendo usuario"
            };
            res.status(500);
            res.send(response);
          } else {
            if (body.length <=0){
              response = {
                "msg" : "logout incorrecto"
              };
              res.status(404);
              res.send(response);
            }else{
              var putBody = '{"$unset":{"logged":""}}';
              httpClient.put("user?" + query + "&" + mLabApiKey, JSON.parse(putBody),function(err, resPutMlab, body){
                if (err)
                {
                  response = {
                      "msg" : "Error marcando usuario logado"
                  };
                  res.status(500);
                } else {
                  console.log ("logout correcto");
                  response = {
                      "msg" : "logout correcto"
                  };
                }
                res.send(response);
              });
            }
          }
        }
      );
    }
)


app.post ("/apitechu/v1/logout",
  function(req,res){
      console.log("POST /apitechu/v1/logout");
      var id = req.body.id;
      var users = require('./usuarios_login.json');
      var encontrado = false;
      for (user of users)
      {
        if (user.id == id && user.logged)
        {
          encontrado = true;
          delete user.logged;
          writeUserDataToFile(users);
        }
      }
      if (encontrado)
      {
        res.send ({"mensage":"logout correcto", "idUsuario": id});
      }
      else {
        res.send ({"mensage":"login incorrecto"});
      }
    }
)

app.get ("/apitechu/v2/users",
  function(req,res){
      console.log("GET /apitechu/v2/users");
      var httpClient = requestJson.createClient(baseMLabURL);
      console.log("Cliente HTTP creado");
      httpClient.get("user?" +  mLabApiKey,
        function(err, resMLab, body){
          var response = !err?body : {
            "msg" : "Error obteniendo usuarios"
          }
          res.send(response);
        }
      );
  }
);

app.get ("/apitechu/v2/users/:id",
  function(req,res){
      console.log("GET /apitechu/v2/users/:id");
      var id = req.params.id;
      var query = 'q={"id":' + id + '}';
      var httpClient = requestJson.createClient(baseMLabURL);
      console.log("Cliente HTTP creado");
      httpClient.get("user?" +  query + "&" + mLabApiKey,
        function(err, resMLab, body){
          /*var response = !err?body : {
            "msg" : "Error obteniendo usuario"
          }*/
          var response;
          if (err)
          {
            response = {
                "msg" : "Error obteniendo usuario"
            };
            res.status(500);
          } else {
            if (body.length >0){
              response = body
            }
            else {
              response = {
                "msg" : "Usuario no encontrado"
              };
              res.status(404);
            }
          }
          res.send(response);
        }
      );
  }
);

app.get ("/apitechu/v2/users/:id/accounts",
  function(req,res){
      console.log("GET /apitechu/v2/users/:id/account");
      var id = req.params.id;
      var query = 'q={"userId":' + id + '}';
      var httpClient = requestJson.createClient(baseMLabURL);
      console.log("Cliente HTTP creado");
      httpClient.get("account?" +  query + "&" + mLabApiKey,
        function(err, resMLab, body){
          //console.log(body)
          var response;
          if (err)
          {
            response = {
                "msg" : "Error obteniendo cuentas"
            };
            res.status(500);
          } else {
            if (body.length >0){
              response = body;
            }
            else {
              response = {
                "msg" : "Usuario no encontrado"
              };
              res.status(404);
            }
          }
          res.send(response);
        }
      );
  }
);



function writeUserDataToFile(data){
  var jsonUserData = JSON.stringify(data);
  //necesaria la libreria fs para poder generar ficheros
  var fs =require('fs');
  fs.writeFile("./usuarios_login.json",jsonUserData, "utf8",
    function(err){
      if (err){
        console.log(err);
      }else {
        console.log ("datos escritos en archivo");
      }
    }
  );
}
