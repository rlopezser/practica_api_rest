FROM node

#Definimos el directorio de trabajo
WORKDIR /apitechu

#Añadimos el contenido a ese directorio de trabajo
ADD . /apitechu

#Definimos el puerto de trabajo
EXPOSE 3000

#En el directorio de trabajo necesitamos ejecutar un comando npm start
CMD ["npm", "start"]
